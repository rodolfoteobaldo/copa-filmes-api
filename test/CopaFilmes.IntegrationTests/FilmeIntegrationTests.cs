using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using CopaFilme.Test.Core;
using CopaFilmes.Application.ViewModels;
using CopaFilmes.Domain.Filmes;
using FluentAssertions;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using Xunit;

namespace CopaFilmes.IntegrationTests
{
    public class FilmeIntegrationTests
    {
        private const string UrlFilme = "/api/v1/filmes";
        private readonly HttpClient _client;
        private readonly TestServer _server;
        private readonly HttpClient _clientError;
        private readonly TestServer _serverError;
        
        public FilmeIntegrationTests()
        {
            _server = FixtureBuilder.CreateTestServer("appsettings.json");
            _client = _server.CreateClient();
            
            _serverError = FixtureBuilder.CreateTestServer("appsettingsError.json");
            _clientError = _serverError.CreateClient();
        }

        [Fact]
        public async void Filme_BuscarFilmes_RetornoComSucesso()
        {
            var response = await _client.GetAsync(UrlFilme);

            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var content = await response.Content.ReadAsStringAsync();
            var filmes = JsonConvert.DeserializeObject<IEnumerable<FilmeViewModel>>(content);

            filmes.Count().Should().BeGreaterThan(0);
        }
        
        [Fact]
        public async void Filme_BuscarFilmes_RetornoExcecao_FaltaUrlApiFilmes()
        {
            var response = await _clientError.GetAsync(UrlFilme);

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var content = await response.Content.ReadAsStringAsync();

            content.Should().Contain("Variável de ambiente 'URLAPIFILMES' não foi configurada");
        }
    }
}