using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CopaFilme.Test.Core;
using CopaFilmes.Application.ViewModels;
using CopaFilmes.Domain.Filmes;
using FluentAssertions;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using Xunit;

namespace CopaFilmes.IntegrationTests
{
    public class CopaIntegrationTests
    {
        public static readonly JsonSerializerSettings SerializerSettings = new JsonSerializerSettings();
        private const string UrlCopaFilme = "/api/v1/copa-filmes";
        private readonly HttpClient _client;
        private readonly TestServer _server;

        public CopaIntegrationTests()
        {
            _server = FixtureBuilder.CreateTestServer("appsettings.json");
            _client = _server.CreateClient();
        }

        [Fact]
        public async void CopaFilme_GerarCampeonato_RetornoComSucesso()
        {
            var listaFilmes = DataFaker.ObterFilmesPorQuantidade(8);
            var resultadoCopa = await ValidarCopaFilme(listaFilmes);

            resultadoCopa.Campeao.Should().NotBeNull();
            resultadoCopa.ViceCampeao.Should().NotBeNull();
            resultadoCopa.Partidas.Count().Should().BeGreaterThan(0);
        }
        
        [Fact]
        public async void CopaFilme_GerarCampeonato_RetornoComSucesso_VingadoresCampeao()
        {
            var listaFilmes = DataFaker.ObterFilmesCampeaoVingadores();
            var resultadoCopa = await ValidarCopaFilme(listaFilmes);

            var filmeVingadores = DataFaker.Filmes.First(f => f.Id == "tt4154756");
            var filmeOsIncriveis2 = DataFaker.Filmes.First(f => f.Id == "tt3606756");
            resultadoCopa.Campeao.Should().BeEquivalentTo(filmeVingadores);
            resultadoCopa.ViceCampeao.Should().BeEquivalentTo(filmeOsIncriveis2);
            resultadoCopa.Partidas.Count().Should().BeGreaterThan(0);
        }
        
        [Fact]
        public async void CopaFilme_GerarCampeonato_RetornoComSucesso_OsIncriveisCampeao()
        {
            var listaFilmes = DataFaker.ObterFilmesCampeaoOsIncriveis();
            var resultadoCopa = await ValidarCopaFilme(listaFilmes);

            var filmeOsIncriveis = DataFaker.Filmes.First(f => f.Id == "tt0317705");
            var filmeUpgrade = DataFaker.Filmes.First(f => f.Id == "tt6499752");
            resultadoCopa.Campeao.Should().BeEquivalentTo(filmeOsIncriveis);
            resultadoCopa.ViceCampeao.Should().BeEquivalentTo(filmeUpgrade);
            resultadoCopa.Partidas.Count().Should().BeGreaterThan(0);
        }
        
        [Theory]
        [InlineData(3)]
        [InlineData(5)]
        [InlineData(10)]
        public async void CopaFilme_BuscarFilmes_RetornoExcecao_FaltaUrlApiFilmes(int quantidade)
        {
            var listaFilmes = DataFaker.ObterFilmesPorQuantidade(quantidade);
            var response = await _client.PostAsync(UrlCopaFilme, RetornaListaFilmesContent(listaFilmes));

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var content = await response.Content.ReadAsStringAsync();

            content.Should().Contain("Numero de participantes inválido");
        }

        private async Task<CopaResultadoViewModel> ValidarCopaFilme(IEnumerable<Filme> listaFilmes)
        {
            var response = await _client.PostAsync(UrlCopaFilme, RetornaListaFilmesContent(listaFilmes));

            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var content = await response.Content.ReadAsStringAsync();
            var responseData = JsonConvert.DeserializeObject<ResponseDataTest>(content);
            return FixtureBuilder.ToObject<CopaResultadoViewModel>(responseData.data);
        }

        private StringContent RetornaListaFilmesContent(IEnumerable<Filme> filmes) =>
            new StringContent(JsonConvert.SerializeObject(filmes),
                Encoding.UTF8, "application/json");
    }
}