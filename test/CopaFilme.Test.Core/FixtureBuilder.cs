using CopaFilmes.WebApi;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace CopaFilme.Test.Core
{
    public static class FixtureBuilder
    {
        public static TestServer CreateTestServer(string nameAppSettings)
        {
            var builder = new WebHostBuilder()
                .UseEnvironment("Test")
                .ConfigureAppConfiguration((_, config) => 
                    config.AddJsonFile(nameAppSettings, optional: false, reloadOnChange: true))
                .UseStartup<Startup>();
            return new TestServer(builder);
        }
        
        public static T ToObject<T>(object obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}