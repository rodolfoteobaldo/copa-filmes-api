using System;
using System.Linq;
using CopaFilme.Test.Core;
using CopaFilmes.Domain.Copa;
using FluentAssertions;
using Xunit;

namespace CopaFilmes.UnitTests
{
    public class CopaUnitTest
    {
        [Theory]
        [InlineData(3)]
        [InlineData(5)]
        [InlineData(10)]
        public void CopaFilme_GerarCampeonato_GeraExcecao_QuantidadeInvalidaParticipantes(int quantidade)
        {
            var copaFilme = new Copa(DataFaker.ObterFilmesPorQuantidade(quantidade));
            Action copaInvalida = () => copaFilme.GerarCampeonato();

            copaInvalida.Should().Throw<Exception>();
        }

        [Fact]
        public void CopaFilme_GerarCampeonato_GeraComSucesso_CampeaoVingadores()
        {
            var copaFilme = new Copa(DataFaker.ObterFilmesCampeaoVingadores());
            var resultadoFinal = copaFilme.GerarCampeonato();

            var filmeVingadores = DataFaker.Filmes.First(f => f.Id == "tt4154756");
            resultadoFinal.Campeao.Should().BeEquivalentTo(filmeVingadores);
        }
        
        [Fact]
        public void CopaFilme_GerarCampeonato_GeraComSucesso_CampeaoOsIncriveis()
        {
            var copaFilme = new Copa(DataFaker.ObterFilmesCampeaoOsIncriveis());
            var resultadoFinal = copaFilme.GerarCampeonato();

            var filmeOsIncriveis = DataFaker.Filmes.First(f => f.Id == "tt0317705");
            resultadoFinal.Campeao.Should().BeEquivalentTo(filmeOsIncriveis);
        }
        
        [Fact]
        public void CopaFilme_GerarCampeonato_GeraComSucesso_QuantidadePartidasIgual_1()
        {
            var copaFilme = new Copa(DataFaker.ObterFilmesPorQuantidade(2));
            var resultadoFinal = copaFilme.GerarCampeonato();

            resultadoFinal.Partidas.Count().Should().Be(1);
        }

        [Fact]
        public void CopaFilme_GerarCampeonato_GeraComSucesso_QuantidadePartidasIgual_3()
        {
            var copaFilme = new Copa(DataFaker.ObterFilmesPorQuantidade(4));
            var resultadoFinal = copaFilme.GerarCampeonato();
            
            resultadoFinal.Partidas.Count().Should().Be(3);
        }
        
        [Fact]
        public void CopaFilme_GerarCampeonato_GeraComSucesso_QuantidadePartidasIgual_7()
        {
            var copaFilme = new Copa(DataFaker.ObterFilmesPorQuantidade(8));
            var resultadoFinal = copaFilme.GerarCampeonato();
            
            resultadoFinal.Partidas.Count().Should().Be(7);
        }
        
        [Fact]
        public void CopaFilme_GerarCampeonato_GeraComSucesso_QuantidadePartidasIgual_15()
        {
            var copaFilme = new Copa(DataFaker.ObterFilmesPorQuantidade(16));
            var resultadoFinal = copaFilme.GerarCampeonato();
            
            resultadoFinal.Partidas.Count().Should().Be(15);
        }
    }
}