# API da aplicação Copa Filmes

API da aplicação Copa Filmes

## O que está implementado:

- .NET Core 2.2
- AutoMapper
- Swagger UI
- Testes Unitários e de Integração

## Como utilizar:
- Será necesário instalar a última versão do .NET Core [SDK](https://dotnet.microsoft.com/download).
- Clonar o projeto com o comando:
```sh
$ git clone https://gitlab.com/rodolfoteobaldo/copa-filmes-api.git
```

- Executar a API com o comando:
```sh
$ dotnet restore
$ dotnet run --project src/CopaFilmes.WebApi
```
> A API está configurada padrão para rodar na url `https://localhost:5001/`

- Executar os teste com o comando:
```sh
$ dotnet test
```

- O Swagger estará acessível na URL:
```
https://{URL:PORTA}/swagger
```
> URL = Servidor onde está rodando a aplicação

> PORTA = Porta do servidor onde está rodando a aplicação
