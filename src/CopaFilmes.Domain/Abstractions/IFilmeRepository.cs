using System.Collections.Generic;
using System.Threading.Tasks;
using CopaFilmes.Domain.Filmes;

namespace CopaFilmes.Domain.Abstractions
{
    public interface IFilmeRepository
    {
        Task<IEnumerable<Filme>> ObterFilmes();
    }
}