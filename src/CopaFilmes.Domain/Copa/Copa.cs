using System;
using System.Collections.Generic;
using System.Linq;
using CopaFilmes.Domain.Filmes;
using CopaFilmes.Domain.Properties;

namespace CopaFilmes.Domain.Copa
{
    public class Copa
    {
        private readonly IEnumerable<Filme> _filmes;

        public Copa(IEnumerable<Filme> filmes)
        {
            _filmes = filmes;
        }
        
        public CopaResultado GerarCampeonato()
        {
            ValidarCampeonato();
                
            var primeiraRodada = new PrimeiraRodada(_filmes).RealizarPrimeiraRodada();
            var rodada = new Rodada(primeiraRodada.Vencedores, primeiraRodada.PartidasCopa).RealizarRodadas();
            return new CopaResultado(rodada.CopaFinal.Campeao, rodada.CopaFinal.ViceCampeao, rodada.PartidasCopa);
        }
        
        private void ValidarCampeonato()
        {
            if(!QuantidadeCorretaTimes(_filmes.Count()))
                throw new Exception(Resources.ValidacaoNumeroParticipantes);
        }

        private bool QuantidadeCorretaTimes(int quantidade)
        {
            var quociente = quantidade / 2;
            var resto = quantidade % 2;

            while (quociente != 1 && resto == 0)
            {
                var valor = quociente;
                quociente = valor / 2;
                resto = valor % 2;
            }

            if (quociente == 1 && resto == 0)
                return true;
            
            return false;
        }
    }
}