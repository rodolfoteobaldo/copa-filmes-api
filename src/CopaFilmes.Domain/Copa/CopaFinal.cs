using CopaFilmes.Domain.Filmes;

namespace CopaFilmes.Domain.Copa
{
    public class CopaFinal
    {
        public CopaFinal(Filme campeao, Filme viceCampeao)
        {
            Campeao = campeao;
            ViceCampeao = viceCampeao;
        }
        
        public Filme Campeao { get; }
        public Filme ViceCampeao { get; }
    }
}