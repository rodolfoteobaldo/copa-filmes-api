using CopaFilmes.Domain.Filmes;

namespace CopaFilmes.Domain.Copa
{
    public class CopaPartida
    {
        public CopaPartida(Filme vencedor, Filme perdedor, int rodada)
        {
            Vencedor = vencedor;
            Perdedor = perdedor;
            Rodada = rodada;
        }
        
        public Filme Vencedor { get; private set; }
        public Filme Perdedor { get; private set; }
        public int Rodada { get; private set; }
    }
}