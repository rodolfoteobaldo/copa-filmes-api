using System.Collections.Generic;
using CopaFilmes.Domain.Filmes;

namespace CopaFilmes.Domain.Copa
{
    public class CopaResultado
    {
        public CopaResultado(Filme campeao, Filme viceCampeao, IEnumerable<CopaPartida> partidas)
        {
            Campeao = campeao;
            ViceCampeao = viceCampeao;
            Partidas = partidas;
        }
        
        public Filme Campeao { get; set; }
        public Filme ViceCampeao { get; set; }
        public IEnumerable<CopaPartida> Partidas { get; set; }
    }
}