using System.Collections.Generic;
using System.Linq;
using CopaFilmes.Domain.Filmes;

namespace CopaFilmes.Domain.Copa
{
    public class Partida
    {
        public Filme Vencedor { get; set; }
        public Filme Perdedor { get; set; }

        public Partida(Filme filmeMandante, Filme filmeVisitante) => 
            RealizarPartida(new List<Filme>() {filmeMandante, filmeVisitante});

        private void RealizarPartida(IEnumerable<Filme> filmes)
        {
            var partida = filmes.OrderByDescending(f => f.Nota).ThenBy(f => f.Titulo);
            Vencedor = partida.First();
            Perdedor = partida.Last();
        }
    }
}