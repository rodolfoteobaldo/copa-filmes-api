using System.Collections.Generic;
using System.Linq;
using CopaFilmes.Domain.Filmes;

namespace CopaFilmes.Domain.Copa
{
    public class Rodada
    {
        private readonly IEnumerable<Filme> _filmes;
        private const int NumeroRodada = 2;
        public List<CopaPartida> PartidasCopa { get; set; }
        public CopaFinal CopaFinal { get; set; }
        
        public Rodada(IEnumerable<Filme> filmes, IEnumerable<CopaPartida> partidas)
        {
            _filmes = filmes;
            PartidasCopa = new List<CopaPartida>(partidas);
        }

        public Rodada RealizarRodadas()
        {
            ObterVencedores(new List<Filme>(_filmes), NumeroRodada);
            return this;
        }
        
        private void ObterVencedores(IList<Filme> filmes, int numeroRodada)
        {
            if(VerificaSeExisteCampeao(filmes)) return;
            
            var filmeMandante = filmes.First();
            var filmeVisitante = filmes.Skip(1).First();
            
            var partida = new Partida(filmeMandante, filmeVisitante);
            PartidasCopa.Add(new CopaPartida(partida.Vencedor, partida.Perdedor, numeroRodada));

            filmes.Remove(filmeMandante);
            filmes.Remove(filmeVisitante);

            if(filmes.Count == 0)
                ObterVencedores(PartidasCopa.Where(p => p.Rodada == numeroRodada).Select(pr => pr.Vencedor).ToList(), ++numeroRodada);

            if(filmes.Count > 1)
                ObterVencedores(filmes, numeroRodada);
        }

        private bool VerificaSeExisteCampeao(IList<Filme> filmes)
        {
            if (filmes.Count != 1)
                return false;
                
            var final = PartidasCopa.Last();
            CopaFinal = new CopaFinal(final.Vencedor, final.Perdedor);
            return true;
        }
    }
}