using System.Collections.Generic;
using System.Linq;
using CopaFilmes.Domain.Filmes;

namespace CopaFilmes.Domain.Copa
{
    public class PrimeiraRodada
    {
        private readonly IEnumerable<Filme> _filmes;
        public List<CopaPartida> PartidasCopa { get; set; }
        public List<Filme> Vencedores { get; set; }

        public PrimeiraRodada(IEnumerable<Filme> filmes)
        {
            _filmes = filmes;
            PartidasCopa = new List<CopaPartida>();
            Vencedores = new List<Filme>();
        }

        public PrimeiraRodada RealizarPrimeiraRodada()
        {
            Vencedores.AddRange(ObterVencedores(new List<Filme>(_filmes).OrderBy(f => f.Titulo).ToList()));
            return this;
        }

        private IEnumerable<Filme> ObterVencedores(IList<Filme> filmes)
        {
            var numeroRodada = 1;
            var listaVencedores = new List<Filme>();

            var filmeMandante = filmes.First();
            var filmeVisitante = filmes.Last();

            var partida = new Partida(filmeMandante, filmeVisitante);
            PartidasCopa.Add(new CopaPartida(partida.Vencedor, partida.Perdedor, numeroRodada));
            listaVencedores.Add(partida.Vencedor);

            filmes.Remove(filmeMandante);
            filmes.Remove(filmeVisitante);

            if (filmes.Any())
                listaVencedores.AddRange(ObterVencedores(filmes));

            return listaVencedores;
        }
    }
}