using CopaFilmes.Application.Abstractions;
using CopaFilmes.Application.Services;
using CopaFilmes.Domain.Abstractions;
using CopaFilmes.Infra.Data.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace CopaFilmes.Infra.CrossCutting.IoC
{
    public static class NativeInjectorBootStrapper
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<IFilmeRepository, FilmeRepository>();
            
            services.AddScoped<IFilmeService, FilmeService>();
            services.AddScoped<ICopaService, CopaService>();
        }
    }
}