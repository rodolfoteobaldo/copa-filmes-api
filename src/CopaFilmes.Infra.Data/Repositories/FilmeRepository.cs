using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CopaFilmes.Domain.Abstractions;
using CopaFilmes.Domain.Core.Constants;
using CopaFilmes.Domain.Core.HttpClientRequest;
using CopaFilmes.Domain.Filmes;
using CopaFilmes.Infra.Data.Properties;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace CopaFilmes.Infra.Data.Repositories
{
    public class FilmeRepository : IFilmeRepository
    {
        private readonly IConfiguration _configuration;

        public FilmeRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        
        public async Task<IEnumerable<Filme>> ObterFilmes()
        {
            string urlFilmes = Convert.ToString(_configuration.GetSection(EnvironmentConstants.UrlApiFilmes)?.Value);
            if(string.IsNullOrEmpty(urlFilmes))
                throw new ArgumentException(Resources.ValidacaoVariavelAmbienteUrlApiFilmes);

            var contentReponse = await HttpClientRequest.GetContentAsStringAsync(urlFilmes);
            return JsonConvert.DeserializeObject<IEnumerable<Filme>>(contentReponse);
            
        }
    }
}