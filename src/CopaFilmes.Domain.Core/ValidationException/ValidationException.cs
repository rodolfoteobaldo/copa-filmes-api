namespace CopaFilmes.Domain.Core.ValidationException
 {
    public class ValidationException
    {
        public ValidationException(int statusCode, string message)
        {
            StatusCode = statusCode;
            Message = message;
        }
        
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}