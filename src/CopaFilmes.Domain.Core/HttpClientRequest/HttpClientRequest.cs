using System;
using System.Net.Http;
using System.Threading.Tasks;
using CopaFilmes.Domain.Core.Properties;

namespace CopaFilmes.Domain.Core.HttpClientRequest
{
    public static class HttpClientRequest
    {
        public static async Task<string> GetContentAsStringAsync(string url)
        {
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                    return await response.Content.ReadAsStringAsync();
            }

            throw new Exception(Resources.ValidacaoDadosRequisicao);
        }
    }
}