using CopaFilmes.WebApi.Properties;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace CopaFilmes.WebApi.Configurations
{
    public static class SwaggerConfiguration
    {
        public static void AddSwaggerConfig(this IServiceCollection services)
        {
            services.AddSwaggerGen(s =>
            {
                s.SwaggerDoc(Resources.VersaoApi, new Info
                {
                    Version = Resources.VersaoApi,
                    Title = Resources.TituloApi,
                    Description = Resources.DescricaoApi,
                    Contact = new Contact
                    {
                        Name = "Rodolfo Martins Teobaldo", 
                        Email = "rodolfoteobaldo@gmail.com", 
                        Url = "https://gitlab.com/users/rodolfoteobaldo/projects"
                    },
                });
            });
        }
    }
}