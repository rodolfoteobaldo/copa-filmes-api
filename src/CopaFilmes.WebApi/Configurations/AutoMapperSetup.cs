using System;
using AutoMapper;
using CopaFilmes.Application.AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace CopaFilmes.WebApi.Configurations
{
    public static class AutoMapperSetup
    {
        public static void AddAutoMapperSetup(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddAutoMapper();
            AutoMapperConfig.RegisterMappings();
        }
    }
}