using System.Net;
using CopaFilmes.Domain.Core.ValidationException;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace CopaFilmes.WebApi.Configurations
{
    public static class ValidationExceptionHandler
    {
        public static void ConfigureValidationExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    context.Response.StatusCode = (int) HttpStatusCode.BadRequest;
                    context.Response.ContentType = "application/json";

                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature != null)
                    {
                        var validation = new ValidationException(context.Response.StatusCode,
                            contextFeature.Error.Message);
                        await context.Response.WriteAsync(JsonConvert.SerializeObject(validation));
                    }
                });
            });
        }
    }
}