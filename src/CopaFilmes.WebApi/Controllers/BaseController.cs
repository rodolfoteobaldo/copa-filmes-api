using Microsoft.AspNetCore.Mvc;

namespace CopaFilmes.WebApi.Controllers
{
    public abstract class BaseController : ControllerBase
    {
        protected new IActionResult Response(object result = null)
        {
            return Ok(new
            {
                success = true,
                data = result
            });
        }
    }
}