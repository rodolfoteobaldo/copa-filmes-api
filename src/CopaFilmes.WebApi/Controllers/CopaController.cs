using System.Collections.Generic;
using System.Threading.Tasks;
using CopaFilmes.Application.Abstractions;
using CopaFilmes.Application.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace CopaFilmes.WebApi.Controllers
{
    public class CopaController : BaseController
    {
        private readonly ICopaService _copaService;

        public CopaController(ICopaService copaService)
        {
            _copaService = copaService;
        }
        
        [HttpPost]
        [Route("copa-filmes")]
        public async Task<IActionResult> Post([FromBody]IList<FilmeViewModel> filmes)
            => Response(await _copaService.GerarCampeonato(filmes));
    }
}