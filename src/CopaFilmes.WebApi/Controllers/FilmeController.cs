using System.Collections.Generic;
using System.Threading.Tasks;
using CopaFilmes.Application.Abstractions;
using CopaFilmes.Application.ViewModels;
using CopaFilmes.Domain.Filmes;
using Microsoft.AspNetCore.Mvc;

namespace CopaFilmes.WebApi.Controllers
{
    public class FilmeController : BaseController
    {
        private readonly IFilmeService _filmeService;

        public FilmeController(IFilmeService filmeService)
        {
            _filmeService = filmeService;
        }
        
        [HttpGet]
        [Route("filmes")]
        public async Task<ActionResult<IEnumerable<FilmeViewModel>>> ObterFilmes() => 
            Ok(await _filmeService.ObterFilmes());
    }
}