using System.Collections.Generic;
using System.Threading.Tasks;
using CopaFilmes.Application.ViewModels;

namespace CopaFilmes.Application.Abstractions
{
    public interface ICopaService
    {
        Task<CopaResultadoViewModel> GerarCampeonato(IEnumerable<FilmeViewModel> filmes);
    }
}