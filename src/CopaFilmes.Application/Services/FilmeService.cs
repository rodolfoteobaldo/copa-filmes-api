using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CopaFilmes.Application.Abstractions;
using CopaFilmes.Application.ViewModels;
using CopaFilmes.Domain.Abstractions;

namespace CopaFilmes.Application.Services
{
    public class FilmeService : IFilmeService
    {
        private readonly IMapper _mapper;
        private readonly IFilmeRepository _filmeRepository;

        public FilmeService(IMapper mapper, IFilmeRepository filmeRepository)
        {
            _mapper = mapper;
            _filmeRepository = filmeRepository;
        }
        
        public async Task<IEnumerable<FilmeViewModel>> ObterFilmes() 
            => _mapper.Map<IEnumerable<FilmeViewModel>>(await _filmeRepository.ObterFilmes());
    }
}