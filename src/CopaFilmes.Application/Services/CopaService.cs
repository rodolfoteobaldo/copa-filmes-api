using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CopaFilmes.Application.Abstractions;
using CopaFilmes.Application.ViewModels;
using CopaFilmes.Domain.Copa;
using CopaFilmes.Domain.Filmes;

namespace CopaFilmes.Application.Services
{
    public class CopaService : ICopaService
    {
        private readonly IMapper _mapper;

        public CopaService(IMapper mapper)
        {
            _mapper = mapper;
        }
        
        public Task<CopaResultadoViewModel> GerarCampeonato(IEnumerable<FilmeViewModel> filmes)
        {
            return Task.Run(() =>
            {
                var resultadoCopa = new Copa(_mapper.Map<IEnumerable<Filme>>(filmes)).GerarCampeonato();
                return _mapper.Map<CopaResultadoViewModel>(resultadoCopa);
            });
        }
    }
}