namespace CopaFilmes.Application.ViewModels
{
    public class CopaPartidaViewModel
    {
        public FilmeViewModel Vencedor { get; set; }
        public FilmeViewModel Perdedor { get; set; }
        public int Rodada { get; set; }
    }
}