using System.Collections.Generic;

namespace CopaFilmes.Application.ViewModels
{
    public class CopaResultadoViewModel
    {
        public FilmeViewModel Campeao { get; set; }
        public FilmeViewModel ViceCampeao { get; set; }
        public IEnumerable<CopaPartidaViewModel> Partidas { get; set; }
    }
}