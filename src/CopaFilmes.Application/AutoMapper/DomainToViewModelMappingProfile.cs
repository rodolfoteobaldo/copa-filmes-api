using AutoMapper;
using CopaFilmes.Application.ViewModels;
using CopaFilmes.Domain.Copa;
using CopaFilmes.Domain.Filmes;

namespace CopaFilmes.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Filme, FilmeViewModel>();
            CreateMap<CopaPartida, CopaPartidaViewModel>();
            CreateMap<CopaResultado, CopaResultadoViewModel>();
        }
    }
}