using AutoMapper;
using CopaFilmes.Application.ViewModels;
using CopaFilmes.Domain.Copa;
using CopaFilmes.Domain.Filmes;

namespace CopaFilmes.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<FilmeViewModel, Filme>();
            CreateMap<CopaPartidaViewModel, CopaPartida>();
            CreateMap<CopaResultadoViewModel, CopaResultado>();
        }
    }
}